<?php
    require('csv-utils.php');

    $success = removeFromCsv('articles.csv', $_GET['id']);
    if($success === FALSE) {
        echo("Erreur à la suppression de l’article " . $_GET['id']);
    } else {
        header('Location: admin.php');
    }
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>CraieAtor</title>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="menu.css">
</head>

<body>
<div class="shade">
	<div class="blackboard">
		<div class="form">

        </div>
    </div>
</div>

<div class="area"></div>
<nav class="main-menu">
  <ul>
    <?php foreach($articles as $art): ?>
      <li>
        <a href="page.php?id=<?= $art['id'] ?>">
        <i class="fa fa-magic fa-2x"></i>
        <?= $art['title'] ?>
        </a>
      </li>
    <?php endforeach; ?>
  </ul>

  <ul class="logout">
    <li>
      <a href="create_article.php">
      <i class="fa fa-pencil-square-o fa-2x"></i>
        <span class="nav-text">
          Créer un article
        </span>
      </a>
    </li>
    
    <li>
      <a href="admin.php">
      <i class="fa fa-sitemap fa-2x"></i>
        <span class="nav-text">
          Panel admin
        </span>
      </a>
    </li>
    
    <li>
      <a href="index.php">
      <i class="fa fa-home fa-2x"></i>
        <span class="nav-text">
          Accueil
        </span>
      </a>
    </li>
    
    <li>
      <a href="who.php">
      <i class="fa fa-user fa-2x"></i>
        <span class="nav-text">
          Qui sommes-nous ?
        </span>
      </a>
    </li>
  </ul>
</nav>
</body>
</html>