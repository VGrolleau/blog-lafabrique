<?php
  require('csv-utils.php');

  $articles = parseCsv('articles.csv');

  session_start();
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>CraieAtor</title>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="menu.css">
</head>

<body>
<div class="shade">
		<div class="blackboard">
				<div class="form">
          <h1>CraieAtor</h1>
            <h2 id="hIntro">
              Quel avenir pour la craie ?
            </h2>

            <p class="pIntro">
              Quand la EdTech tue la craie : Les livres sont remplacés par les tablettes électroniques, <br> les tableaux par les écrans…
            </p>
            <p class="pIntro">
              Mais que va devenir la craie ?!
            </p>
            <p class="pIntro">
              Perdurera-t-elle à travers une possible nostalgie de son passage crissant sur le tableau noir ? 
            </p>
        </div>
    </div>
</div>

<div class="area"></div>
<nav class="main-menu">
  <ul>
    <?php foreach($articles as $art): ?>
      <li>
        <a href="page.php?id=<?= $art['id'] ?>">
        <i class="fa fa-magic fa-2x"></i>
        <?= $art['title'] ?>
        </a>
      </li>
    <?php endforeach; ?>
  </ul>

  <ul class="logout">
    <li>
      <a href="create_article.php">
      <i class="fa fa-pencil-square-o fa-2x"></i>
        <span class="nav-text">
          Créer un article
        </span>
      </a>
    </li>
    
    <li>
      <a href="admin.php">
      <i class="fa fa-sitemap fa-2x"></i>
        <span class="nav-text">
          Panel admin
        </span>
      </a>
    </li>
    
    <li class="log">
      <i class="fa fa-power-off fa-2x"></i>
        <span class="nav-text">
          <?php if(isset($_SESSION['id'])) : ?>
              <a class="log" href="logout.php">
              <?php echo "Deconnexion";?>
              </a>
            <?php else : ?>
              <a class="log" href="login.php">
              <?php echo "Connexion";?>
              </a>
            <?php endif; ?>
        </span>
    </li>
    
    <li>
      <a href="index.php">
      <i class="fa fa-home fa-2x"></i>
        <span class="nav-text">
          Accueil
        </span>
      </a>
    </li>
    
    <li>
      <a href="who.php">
      <i class="fa fa-user fa-2x"></i>
        <span class="nav-text">
          Qui sommes-nous ?
        </span>
      </a>
    </li>
  </ul>
</nav>
</body>
</html>