<?php
    require('csv-utils.php');

    $articles = parseCsv('articles.csv');

    session_start();

    if(!isset($_SESSION['id'])) {
        header("Location: login.php");
    }
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>CraieAtor</title>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="menu.css">
</head>

<body>
<div class="shade">
	<div class="blackboard">
		<div class="form">
            <h2>Administration des articles</h2>

            <a href="create_article.php"><button>Nouvel article</button></a>

            <ul>
                <?php foreach($articles as $art): ?>
                    <br><li>
                        <?= $art['title'] ?> <br>
                        <a href="edit_article.php?id=<?= $art['id']?>"><button>Modifier</button></a><a href="delete_article.php?id=<?= $art['id']?>"><button>Supprimer</button></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>

<div class="area"></div>
<nav class="main-menu">
  <ul>
    <?php foreach($articles as $art): ?>
      <li>
        <a href="page.php?id=<?= $art['id'] ?>">
        <i class="fa fa-magic fa-2x"></i>
        <?= $art['title'] ?>
        </a>
      </li>
    <?php endforeach; ?>
  </ul>

  <ul class="logout">
    <li>
      <a href="create_article.php">
      <i class="fa fa-pencil-square-o fa-2x"></i>
        <span class="nav-text">
          Créer un article
        </span>
      </a>
    </li>
    
    <li>
      <a href="admin.php">
      <i class="fa fa-sitemap fa-2x"></i>
        <span class="nav-text">
          Panel admin
        </span>
      </a>
    </li>
    
    <li>
      <a href="logout.php">
      <i class="fa fa-power-off fa-2x"></i>
        <span class="nav-text">
          Déconnexion
        </span>
      </a>
    </li>
    
    <li>
      <a href="index.php">
      <i class="fa fa-home fa-2x"></i>
        <span class="nav-text">
          Accueil
        </span>
      </a>
    </li>
    
    <li>
      <a href="who.php">
      <i class="fa fa-user fa-2x"></i>
        <span class="nav-text">
          Qui sommes-nous ?
        </span>
      </a>
    </li>
  </ul>
</nav>
</body>
</html>